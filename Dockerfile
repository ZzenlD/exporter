FROM alpine:latest
MAINTAINER ZzenlD

RUN apk add --no-cache bash curl jq git python3 tzdata && \
    cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime && echo "Europe/Berlin" >  /etc/timezone && \
    apk del --no-cache tzdata && \
    #    apk add --no-cache --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ getmail && \
    #    git clone https://github.com/pectojin/duplicati-client /usr/local/duc && \
    #    pip3 install -r /usr/local/duc/requirements.txt && \
    #    ln -s /usr/local/duc/duplicati_client.py /usr/bin/duc && \
    mkdir -p /exporter/data && \
    echo '@daily cd /exporter && bash export.sh' > /var/spool/cron/crontabs/root

WORKDIR /exporter
COPY agents agents
COPY export.sh export.sh
ENV BACKUP_PATH="/exporter/data" USERID=1000 GROUPID=1000

VOLUME /exporter/data

CMD echo '********* Starting Exporter *********' && \
    bash export.sh && \
    crond -f
