# ZzenlD/Exporter
Tool for regular, automatic export of different configurations based on the stored agents.


## Usage

```console
$ podman create \
    --name=Exporter \
#    -e USERID=1000 \
#    -e GROUPID=1000 \
    -v <host path>:/exporter/data \
    registry.gitlab.com/zzenld/exporter
```

Or execute directly with:

```console
$ podman run -d -v <host path>:/exporter/data --name Exporter registry.gitlab.com/zzenld/exporter
```


## Parameters

* `-v /exporter/data` - path on the host system

It is based on alpine:latest, for shell access whilst the container is running do `podman exec -it Exporter /bin/bash`.


## Running Exporter in read-only mode
To run Exporter in read-only mode, you will need to mount a volume to every location where Exporter writes information. The default Exporter configuration requires write access only to `/exporter/data`. This can be easily accomplished by running Exporter as follows:

```console
$ podman run -d --read-only -v <host path>:/exporter/data --name Exporter registry.gitlab.com/zzenld/exporter
```


## Agent Configuration
Create a configuration file config.env in the mounted directory on the host system and copy all agents listed below to it

### GitLab
Searches for all repositories of a user and exports them. Default instance is `gitlab.com`.

```yaml
#GITLAB_HOST=gitlab.com
GITLAB_USERNAME=<your username>
GITLAB_PASSWORD=<your access token>
```

### Nextcloud
Exports all specified calendars and contacts, default is `personal` and `contacts`. Tasks to be exported must be also specified in the variable `NEXTCLOUD_CALENDARS`.

```yaml
NEXTCLOUD_HOST=<nextcloud host>
NEXTCLOUD_USERNAME=<your username>
NEXTCLOUD_PASSWORD=<your access token>
#NEXTCLOUD_CALENDARS=("personal")
#NEXTCLOUD_ADDRESSBOOKS=("contacts")
```

### QNAP
Exports qnap configuration.

```yaml
QNAP_HOST=<qnap host>
QNAP_USERNAME=<your username>
QNAP_PASSWORD=<your password>
```

### FritzBox
Exports FritzBox configuration, default host is `fritz.box` with username `admin`.

```yaml
#FRITZBOX_HOST=fritz.box
#FRITZBOX_USERNAME=admin
FRITZBOX_PASSWORD=<your password>
```

### IMAP
Exports all mails from email server, default port is `993`.

```yaml
IMAP_USERNAME=<your username>
IMAP_PASSWORD=<your password>
#IMAP_PORT=993
```

### Duplicati
Export all configurations of backup jobs.

```yaml
DUPLICATI_HOST=<duplicati host>
```

### Custom
Exports custom files which not be specified by an agent.

```yaml
CUSTOM_1_URL=<first custom url>
CUSTOM_1_USERNAME=<first username>
CUSTOM_1_PASSWORD=<first password>
CUSTOM_1_FILENAME=<first filename>
#CUSTOM_<n>_URL=<n custom url>
#CUSTOM_<n>_USERNAME=<n username>
#CUSTOM_<n>_PASSWORD=<n password>
#CUSTOM_<n>_FILENAME=<n filename>
```


## Info

* Shell access whilst the container is running: `podman exec -it Exporter /bin/bash`
* To monitor the logs of the container in realtime: `podman logs -f Exporter`
