#!/usr/bin/env bash
source /exporter/data/Exporter/config.env

rm -rf "${BACKUP_PATH}"/Custom 2>/dev/null
if [[ -n "${CUSTOM_1_URL}" ]] && [[ -n "${CUSTOM_1_USERNAME}" ]] && [[ -n "${CUSTOM_1_PASSWORD}" ]] && [[ -n "${CUSTOM_1_FILENAME}" ]]; then
  echo 'custom:'

  # Roll over every Custom
  count=1
  while [[ "$(eval "echo \${CUSTOM_${count}_URL}")" != "" ]]; do
    echo -n "  custom${count}: "

    if curl -ks "$(eval "echo \${CUSTOM_${count}_URL}")" --user "$(eval "echo \${CUSTOM_${count}_USERNAME}")":"$(eval "echo \${CUSTOM_${count}_PASSWORD}")" -o "${BACKUP_PATH}"/Custom/"$(eval "echo \${CUSTOM_${count}_FILENAME}")" --create-dirs; then
      echo 'ok'
    else
      echo 'failed'
    fi
    ((count++))
  done
fi
