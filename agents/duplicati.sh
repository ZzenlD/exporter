#!/usr/bin/env bash
source /exporter/data/Exporter/config.env

rm -rf "${BACKUP_PATH}"/Duplicati 2>/dev/null
if [[ -n "${DUPLICATI_HOST}" ]]; then
  echo 'duplicati:'

  # Export Configuration
  echo -n '  configuration: '
  if duc login --insecure https://"${DUPLICATI_HOST}" 1>/dev/null && duc export --all --output-path="${BACKUP_PATH}"/Duplicati 1>/dev/null && duc logout 1>/dev/null; then
    echo 'ok'
  else
    echo 'failed'
  fi
fi
