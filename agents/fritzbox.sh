#!/usr/bin/env bash
source /exporter/data/Exporter/config.env

rm -rf "${BACKUP_PATH}"/FritzBox 2>/dev/null
if [[ -n "${FRITZBOX_PASSWORD}" ]]; then
  echo 'fritzbox:'

  # Export Configuration
  echo -n '  configuration: '
  if curl -ksfm 20 https://"${FRITZBOX_HOST:-fritz.box}":81/cgi-bin/backup/do_backup.cgi --user "${FRITZBOX_USERNAME:-admin}":"${FRITZBOX_PASSWORD}" -o "${BACKUP_PATH}"/FritzBox/config.xzf --create-dirs; then
    echo 'ok'
  else
    echo 'failed'
  fi
fi
