#!/usr/bin/env bash
source /exporter/data/Exporter/config.env

if [[ -n "${GITLAB_USERNAME}" ]] && [[ -n "${GITLAB_PASSWORD}" ]]; then
  echo -e 'gitlab:\n  repositories:'

  # Export all Repositories
  for repository in $(curl -s --header "PRIVATE-TOKEN: ${GITLAB_PASSWORD}" https://"${GITLAB_HOST:-gitlab.com}"/api/v4/users/"${GITLAB_USERNAME}"/projects | jq -r .[].path); do
    remote_repositories+=("${repository}")
    echo -n "    ${repository}: "
    if [[ -d "${BACKUP_PATH}"/GitLab/"${repository}" ]]; then
      if cd "${BACKUP_PATH}"/GitLab/"${repository}" && git remote update &>/dev/null && cd - &>/dev/null; then
        echo 'updated'
      else
        echo 'failed'
      fi
    else
      if git clone -q --mirror https://"${GITLAB_USERNAME}":"${GITLAB_PASSWORD}"@"${GITLAB_HOST:-gitlab.com}"/"${GITLAB_USERNAME}"/"${repository}" "${BACKUP_PATH}"/GitLab/"${repository}"; then
        echo 'cloned'
      else
        echo 'failed'
      fi
    fi
  done
  for repository in $(ls "${BACKUP_PATH}"/GitLab); do
    if ! [[ " ${remote_repositories[@]} " =~ " ${repository} " ]]; then
      echo -n "    ${repository}: "
      rm -rf "${BACKUP_PATH}"/GitLab/"${repository}"
      echo 'deleted'
    fi
  done
fi
