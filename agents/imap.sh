#!/usr/bin/env bash
source /exporter/data/Exporter/config.env

adduser -h ${BACKUP_PATH}/IMAP -D user 2>/dev/null
rm -rf "${BACKUP_PATH}"/IMAP/* 2>/dev/null
if [[ -n "${IMAP_USERNAME}" ]] && [[ -n "${IMAP_PASSWORD}" ]]; then
  echo -en 'IMAP:\n  ALL: '
  su user -c 'mkdir ~/cur && mkdir ~/new && mkdir ~/tmp'
  cat <<EOF >/tmp/getmailrc
[retriever]
type = SimpleIMAPSSLRetriever
server = ${IMAP_USERNAME#*@}
username = ${IMAP_USERNAME}
password = ${IMAP_PASSWORD}
port = ${IMAP_PORT:-993}
mailboxes = ALL

[destination]
type = Maildir
path = ${BACKUP_PATH}/IMAP/

[options]
verbose = 2
read_all = true
delivered_to = false
received = false
EOF

  # Export mails
  if su user -c 'getmail -g /tmp &>/dev/null'; then
    chmod -R 777 "${BACKUP_PATH}"/IMAP
    echo 'ok'
  else
    echo 'failed'
  fi
fi
