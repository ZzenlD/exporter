#!/usr/bin/env bash
source /exporter/data/Exporter/config.env

if [[ -n "${NEXTCLOUD_HOST}" ]] && [[ -n "${NEXTCLOUD_USERNAME}" ]] && [[ -n "${NEXTCLOUD_PASSWORD}" ]]; then
  echo -e 'nextcloud:\n  apps:'

  # Export all Calendars
  echo '    calendars:'
  for calendar in ${NEXTCLOUD_CALENDARS[*]:-personal}; do
    remote_calendars+=("${calendar}")
    echo -n "      ${calendar}: "
    if curl -s https://"${NEXTCLOUD_HOST}"/remote.php/dav/calendars/"${NEXTCLOUD_USERNAME}"/"${calendar}"?export --user "${NEXTCLOUD_USERNAME}":"${NEXTCLOUD_PASSWORD}" -o "${BACKUP_PATH}"/Nextcloud/calendars/"${calendar}".ics --create-dirs; then
      echo 'exported'
    else
      echo 'failed'
    fi
  done
  for calendar in $(ls -l "${BACKUP_PATH}"/Nextcloud/calendars | awk '{print $9}' | awk -F. '{print $1}' | grep -Ev ^$); do
    if ! [[ " ${remote_calendars[@]} " =~ " ${calendar} " ]]; then
      echo -n "      ${calendar}: "
      rm -rf "${BACKUP_PATH}"/Nextcloud/calendars/"${calendar}"*
      echo 'deleted'
    fi
  done

  # Export all Addressbooks
  echo '    addressbooks:'
  for addressbook in ${NEXTCLOUD_ADDRESSBOOKS[*]:-contacts}; do
    remote_addressbooks+=("${addressbook}")
    echo -n "      ${addressbook}: "
    if curl -s https://"${NEXTCLOUD_HOST}"/remote.php/dav/addressbooks/users/"${NEXTCLOUD_USERNAME}"/"${addressbook}"/?export --user "${NEXTCLOUD_USERNAME}":"${NEXTCLOUD_PASSWORD}" -o "${BACKUP_PATH}"/Nextcloud/addressbooks/"${addressbook}".vcf --create-dirs; then
      echo 'exported'
    else
      echo 'failed'
    fi
  done
  for addressbook in $(ls -l "${BACKUP_PATH}"/Nextcloud/addressbooks | awk '{print $9}' | awk -F. '{print $1}' | grep -Ev ^$); do
    if ! [[ " ${remote_addressbooks[@]} " =~ " ${addressbook} " ]]; then
      echo -n "      ${addressbook}: "
      rm -rf "${BACKUP_PATH}"/Nextcloud/addressbooks/"${addressbook}"*
      echo 'deleted'
    fi
  done

  # Export News
  echo -n '    news: '
  if curl -s https://"${NEXTCLOUD_HOST}"/index.php/apps/news/export/opml --user "${NEXTCLOUD_USERNAME}":"${NEXTCLOUD_PASSWORD}" -o "${BACKUP_PATH}"/Nextcloud/news.opml --create-dirs; then
    echo 'exported'
  else
    echo 'failed'
  fi

  # Export Bookmarks
  echo -n '    bookmarks: '
  if curl -s https://"${NEXTCLOUD_HOST}"/index.php/apps/bookmarks/ --user "${NEXTCLOUD_USERNAME}":"${NEXTCLOUD_PASSWORD}" -o "${BACKUP_PATH}"/Nextcloud/bookmarks.html --create-dirs; then
    echo 'exported'
  else
    echo 'failed'
  fi
fi
