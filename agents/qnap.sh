#!/usr/bin/env bash
source /exporter/data/Exporter/config.env

rm -rf "${BACKUP_PATH}"/QNAP 2>/dev/null
if [[ -n "${QNAP_HOST}" ]] && [[ -n "${QNAP_USERNAME}" ]] && [[ -n "${QNAP_PASSWORD}" ]]; then
  echo 'qnap:'

  # Export Configuration
  echo -n '  configuration: '
  if curl -ks https://"${QNAP_HOST}"/remote.php/dav/calendars/"${QNAP_USERNAME}" --user "${QNAP_USERNAME}":"${QNAP_PASSWORD}" -o "${BACKUP_PATH}"/QNAP/config.csv --create-dirs; then
    echo 'ok'
  else
    echo 'failed'
  fi
fi
