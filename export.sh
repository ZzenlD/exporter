#!/usr/bin/env bash

echo -e "\n**** $(date "+%Y-%m-%d %H:%M:%S") ****"
mkdir -p data/Exporter
if ! [[ -f data/Exporter/config.env ]]; then
  touch data/Exporter/config.env
fi
for script in agents/*.sh; do
  bash "$script"
done
#chown -R "${USERID}":"${GROUPID}" data
