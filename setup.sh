#!/usr/bin/env bash

curl -s https://gitlab.com/ZzenlD/zbfl/-/raw/master/zbfl.sh -o /tmp/zbfl.sh
source /tmp/zbfl.sh
script_name="Exporter"
dependencies=("docker")

prepare_zbfl
{
    init_zbfl

    # Configure container
    write_headline "" "Configuration"
    write_ask "" "Path for persistent data" "DATA_PATH" "$(cd ../ && echo $(pwd)/data && cd - &>/dev/null)"
    if ! [[ -d "${DATA_PATH}" ]]; then
        mkdir -p "${DATA_PATH}"
    fi
    write_ask "0" "Use GitLab" "*USE_GITLAB" "no" "yesno"
    if [[ "${USE_GITLAB}" == yes ]]; then
        write_ask "1" "Host" "*GITLAB_HOST" "gitlab.com" "domain"
        write_ask "" "Username" "*GITLAB_USERNAME"
        write_ask "" "Password" "*GITLAB_PASSWORD"
    fi
    write_ask "0" "Use Nextcloud" "*USE_NEXTCLOUD" "no" "yesno"
    if [[ "${USE_NEXTCLOUD}" == yes ]]; then
        write_ask "1" "Host" "*NEXTCLOUD_HOST" "" "domain"
        write_ask "" "Username" "*NEXTCLOUD_USERNAME"
        write_ask "" "Password" "*NEXTCLOUD_PASSWORD"
    fi
    write_ask "0" "Use QNAP" "*USE_QNAP" "no" "yesno"
    if [[ "${USE_QNAP}" == yes ]]; then
        write_ask "1" "Host" "*QNAP_HOST" "" "domain"
        write_ask "" "Username" "*QNAP_USERNAME"
        write_ask "" "Password" "*QNAP_PASSWORD"
    fi
    write_ask "0" "Use FritzBox" "*USE_FRITZBOX" "no" "yesno"
    if [[ "${USE_FRITZBOX}" == yes ]]; then
        write_ask "1" "Host" "*FRITZBOX_HOST" "fritz.box" "domain"
        write_ask "" "Username" "*FRITZBOX_USERNAME" "admin"
        write_ask "" "Password" "*FRITZBOX_PASSWORD"
    fi
    write_ask "0" "Use IMAP" "*USE_IMAP" "no" "yesno"
    if [[ "${USE_IMAP}" == yes ]]; then
        write_ask "1" "Username" "*IMAP_USERNAME" "" "mail"
        write_ask "" "Password" "*IMAP_PASSWORD"
        write_ask "" "Port" "*IMAP_PORT" "993"
    fi
    write_ask "0" "Use Duplicati" "*USE_DUPLICATI" "no" "yesno"
    if [[ "${USE_DUPLICATI}" == yes ]]; then
        write_ask "1" "Host" "*DUPLICATI_HOST" "" "domain"
    fi
    if ! [[ -d "${DATA_PATH}/Exporter" ]]; then
        mkdir -p "${DATA_PATH}/Exporter"
    fi
    cat >"${DATA_PATH}/Exporter/config.env" <<EOF
GITLAB_HOST=${GITLAB_HOST}
GITLAB_USERNAME=${GITLAB_USERNAME}
GITLAB_PASSWORD=${GITLAB_PASSWORD}
NEXTCLOUD_HOST=${NEXTCLOUD_HOST}
NEXTCLOUD_USERNAME=${NEXTCLOUD_USERNAME}
NEXTCLOUD_PASSWORD=${NEXTCLOUD_PASSWORD}
NEXTCLOUD_CALENDARS=("personal")
NEXTCLOUD_ADDRESSBOOKS=("contacts")
QNAP_HOST=${QNAP_HOST}
QNAP_USERNAME=${QNAP_USERNAME}
QNAP_PASSWORD=${QNAP_PASSWORD}
FRITZBOX_HOST=${FRITZBOX_HOST}
FRITZBOX_USERNAME=${FRITZBOX_USERNAME}
FRITZBOX_PASSWORD=${FRITZBOX_PASSWORD}
IMAP_USERNAME=${IMAP_USERNAME}
IMAP_PASSWORD=${IMAP_PASSWORD}
IMAP_PORT=${IMAP_PORT}
DUPLICATI_HOST=${DUPLICATI_HOST}
EOF

    write_headline "0" "Start up"
    write_topic "" "Starting container zzenld/exporter"
    docker run -d --read-only -v "${DATA_PATH}":/exporter/data --name Exporter zzenld/exporter &>/dev/null
    write_status "$?"

    write_result final
} 2> >(write_log)
